@startuml

title
	Admin : Supprimer une ressource
end title
hide footbox
actor Admin

Admin -> Syst�me : s�l�ctionne liste des cours
Syst�me -> Admin : affiche liste des cours
Admin -> Syst�me : s�l�ctionne un cours
Syst�me -> Admin : affiche le cours
Admin -> Syst�me : choisis une ressource
Syst�me -> Admin : envoie le formulaire de modification de la ressource
Admin -> Syst�me : s�l�ctionne suppression de la ressource
Syst�me -> Syst�me : supprime la ressource
Syst�me -> Admin : confirme la suppression de la ressource
@enduml
