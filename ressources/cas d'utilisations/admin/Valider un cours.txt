Cas validation du cours
@startuml
title
	Admin : Valider un cours
	Sc�nario : Cours accept�
end title

hide footbox
actor Admin

Admin -> Syst�me : s�l�ctionne liste des cours en attente de validation
Syst�me -> Admin : affiche la liste des cours
Admin -> Syst�me : s�l�ctionne valider cours d'un cours de la liste
Syst�me -> Syst�me : valide le cours 
Syst�me -> Admin : confirme la validation du cours
@enduml

Cas refuser cours
@startuml
title
	Admin : Valider un cours
	Sc�nario : Cours refus�
end title

hide footbox
actor Admin

Admin -> Syst�me : s�l�ctionne liste des cours en attente de validation
Syst�me -> Admin : affiche la liste des cours
Admin -> Syst�me : s�l�ctionne refuser cours d'un cours de la liste
Syst�me -> Syst�me : supprime le cours 
Syst�me -> Admin : confirme la suppression du cours
@enduml
