@startuml
title
	Admin : Consulter liste des cours en attente de validation
end title

hide footbox
actor Admin

Admin -> Système : séléctionne liste des cours en attente de validation
Système -> Admin : affiche la liste des cours

@enduml
