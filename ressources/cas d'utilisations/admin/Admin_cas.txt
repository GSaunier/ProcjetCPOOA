@startuml
left to right direction
actor Utilisateur
actor Admin 

Utilisateur <|-- Admin 
Enseignant <|-- Admin

Admin --> (Valider un cours)
Admin --> (Supprimer un utilisateur)
Admin --> (Consulter liste cours en attente de validation)
Admin --> (Consulter liste utilisateurs)
Admin --> (Valider inscription)
@enduml
