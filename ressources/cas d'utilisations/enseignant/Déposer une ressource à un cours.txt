@startuml

title 
	Enseignant : Déposer une ressource à un cours
end title

hide footbox
actor Enseignant

Enseignant-> Système : demande accés à la liste de ses cours
Système -> Système : séléctionne la liste des cours associés à l'enseignant
Système -> Système : trie des cours en fonction de leur état
Système -> Enseignant : envoie de la séléction
Enseignant -> Système : séléctionne un cours
Système -> Enseignant : affiche le cours
Enseignant -> Système : séléctionne ajouter ressource
Système -> Enseignant : envoie du formulaire d'ajout de ressource
Enseignant -> Système : envoie du formulaire complété avec ajout de la ressource
Système -> Système : ajoute la ressource au cours
Système -> Enseignant : confirme l'ajout de la ressource au cours


@enduml
