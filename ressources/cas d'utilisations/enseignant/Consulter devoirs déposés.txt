@startuml

title 
	Enseignant : Consulter devoirs déposés
end title

hide footbox
actor Enseignant

Enseignant-> Système : demande accés à la liste de ses cours
Système -> Système : séléctionne la liste des cours associés à l'enseignant
Système -> Système : trie des cours en fonction de leur état
Système -> Enseignant : envoie de la séléction
Enseignant -> Système : séléctionne un cours
Système -> Enseignant : affiche le cours
Enseignant -> Système : séléctionne liste des devoirs
Système -> Système : séléctionne les devoirs appartenant au cours
Système -> Enseignant : affiche la séléction
Enseignant -> Système : séléctionne le devoir désiré
Système -> Système : séléctionne la liste des étudiants ayant rendu le devoir
Système -> Enseignant : affiche la liste
Enseignant -> Système : séléctionne l'étudiant voulu
Système -> Enseignant : afficher le devoir de l'étudiant

@enduml
