@startuml

title 
	Enseignant : Modifier le contenu d'un cours
end title

hide footbox
actor Enseignant

Enseignant-> Système : demande accés à la liste de ses cours
Système -> Système : séléctionne la liste des cours associés à l'enseignant
Système -> Système : trie des cours en fonction de leur état
Système -> Enseignant : envoie de la séléction
Enseignant -> Système : séléctionne un cours
Système -> Enseignant : affiche le cours
Enseignant -> Système : séléctionne modifier le cours
Système -> Enseignant : envoie du formulaire de modification du cours
Enseignant -> Système : envoie du formulaire complété.
Système -> Système : modifie le contenu du cours
Système -> Enseignant : confirme la modification du cours


@enduml
