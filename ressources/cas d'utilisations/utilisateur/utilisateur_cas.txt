@startuml
left to right direction
actor Utilisateur
actor Etudiant
actor Enseignant
actor Administrateur

Etudiant --|> Utilisateur 
Enseignant --|> Utilisateur 
Administrateur --|> Utilisateur 

Utilisateur --> (Se connecter)
Utilisateur --> (Modifier profil)
Utilisateur --> (S'inscrire)
Utilisateur --> (Se déconnecter)
@enduml
