#ifndef CONTROLE_H
#define CONTROLE_H

#include "ressource.hpp"
#include "definition.h"

class Controle : public Ressource
{

protected :

public :
    Controle(std::string n,std::string d) : Ressource(n,d,CONTROLE){}
private :

};

#endif // CONTROLE_H
