#ifndef RESSOURCE_H
#define RESSOURCE_H
#include <iostream>
#include "definition.h"

class Ressource
{

protected:
     std::string nom;
     std::string date;
     int type;

public:
     Ressource(std::string n,std::string d, int t):nom(n),date(d),type(t){}
     std::string getNom(){return nom;}
     std::string getDate(){return date;}
     std::string toString();

};

#endif // RESSOURCE_H
