#include "ressource.hpp"

std::string Ressource::toString(){
    std::string str;
    switch (type) {
    case TEXTE:
        str = "Texte: " ;
        break;
    case CONTROLE:
        str = "Controle: ";
        break;
    case FICHIER:
        str = "Fichier: ";
        break;
    case DEVOIR:
        str = "Devoir: ";
        break;
    default:
        break;
    }
    str +=  getNom() + " " + getDate();
    return str;
}
