#ifndef TEXTE_H
#define TEXTE_H

#include "ressource.hpp"
#include "definition.h"

class Texte : public Ressource
{

protected :
 std::string contenu;
public :
    Texte(std::string n,std::string d): Ressource(n,d,TEXTE){}
private :

}

#endif // TEXTE_H
