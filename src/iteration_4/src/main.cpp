#include <QCoreApplication>
#include <iostream>
#include "utilisateur.hpp"
#include "etudiant.hpp"
#include "enseignant.hpp"
#include "admin.hpp"
#include "donnees.hpp"
#include "definition.h"
#include "application.h"
#include "controle.hpp"

using namespace std;
/** @example console.cpp
 **
 ** Ce programme teste les classes héritant de @ref utilisateur.
 **
 ** @version @version iteration une
 **
 **/

///
/// @param  argc  nombre de paramètres du programme (non utilisé),
/// @param  argv  paramètres du programme (non utilisé).
///
/// @return  0 (OK).
///
/// @version iteration 2 a

int main(){

    Donnees donnees = Donnees::getInstance();

    Cours * c = new Cours("Coucou","24/11/2013","29/12/17",10);

    c->ajouterRess(new Controle("Examen","23/12/2017"));

    Ressource * ress = c->getRessource(0);

    cout << ress->toString() << endl;

    return 0;
}
