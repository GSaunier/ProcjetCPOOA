#include "donnees.hpp"

// constructeur qui ajoute 3 utilisateurs
Donnees::Donnees()
{
    Donnees::ajouterUtilisateur(new Etudiant("elyautt","coucou"));
    Donnees::ajouterUtilisateur(new Enseignant("guyome","hello"));
    Donnees::ajouterUtilisateur(new Admin("arno", "salut"));
}

// ajouter un utilisateur dans le tableau
void Donnees::ajouterUtilisateur(Utilisateur *user)
{
    Donnees::tabUser.push_back(user);
}

bool Donnees::verifMdp(std::string nom, std::string mdp)
{
    bool trouver = false;
    // iteration sur les utilisateurs de tabUser
    for(int unsigned i = 0; i<Donnees::tabUser.size() ; i++) {
        Utilisateur * user = Donnees::getUtilisateur(i);
        // si on trouve le bon utilisateur
        if(user->getNom() == nom){
            if(user->getMdp() == mdp) {
                trouver = true;
            }
        }
    }
    return trouver;
}
// methode pour connecter un utilisateur, retourne l'utilisateur connecté
Utilisateur * Donnees::connexion(std::string nom, std::string mdp)
{
    Utilisateur *user;

    // Recherche de l'utilisateur dans le tableau d'utilisateur
    for(int unsigned i = 0; i<Donnees::tabUser.size() ; i++) {
        user = Donnees::getUtilisateur(i);
        if(user->getNom() == nom){
            if(user->getMdp() == mdp){
                i = Donnees::tabUser.size();
            }
        }
    }
    return user;
}
// methode pour inscrire un utilisateur
void Donnees::inscription(std::string nom, std::string mdp, int type)
{
    Utilisateur * user;

    switch(type){
        case ETUDIANT :
            user = new Etudiant(nom,mdp);
            Donnees::ajouterUtilisateur(user);
            break;
        case ENSEIGNANT :
            user = new Enseignant(nom,mdp);
            Donnees::ajouterUtilisateur(user);
            break;
        case ADMIN :
            user = new Admin(nom,mdp);
            Donnees::ajouterUtilisateur(user);
            break;
        default :
            break;
    }
}

// Retourne uniquement la liste des cours valides
std::vector<Cours*> Donnees::getListeCoursValide(){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(tabCours[i]->isValide()){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}

// Retourne uniquement la liste des cours en attentes
std::vector<Cours *> Donnees::getListeCoursAttente(){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(!tabCours[i]->isValide()){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}

// // Retourne uniquement la liste des cours d'un enseignant
std::vector<Cours*> Donnees::getCoursEnseignant(std::string nom){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(tabCours[i]->getNomEnseignant() == nom){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}
