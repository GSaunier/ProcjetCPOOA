@startuml

title Diagramme de classe

hide empty members

package "user" #DDDDDD{
    abstract class Utilisateur {
        - int id
        - String nom
        --
        .. Constructeur ..
        + Utilisateur()
        + Utilisateur(String nom)
        .. M�thode ..
        + void deconnexion()
    }
    class Etudiant{
    }
    class Enseignant{
    }
    class Admin{
    }
    
    
    Utilisateur <|-- Enseignant
    Utilisateur <|-- Etudiant
    Utilisateur <|-- Admin

}

class Session{
        - static Session instance
        - Utilisateur[] tabUser
        --
        .. Constructeur ..
        - Session()
        .. M�thode pour Singleton ..
        + Session getInstance()
        .. M�thode ..
        + Utilisateur connexion(String nom, String mdp)
        + Boolean verifMdp(String nom, String mdp)
        + void inscription(String nom, String mdp)
}

note right of Session #FFAAAA
    Design Pattern :
    <b>Singleton</b>
    end note

class Application

Application -- "1" Utilisateur

@enduml

