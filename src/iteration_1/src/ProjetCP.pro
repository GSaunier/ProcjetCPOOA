QT += core
QT -= gui

CONFIG += c++11

TARGET = ProjetCP
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    application.cpp \
    session.cpp

HEADERS += \
    admin.hpp \
    enseignant.hpp \
    etudiant.hpp \
    session.hpp \
    utilisateur.hpp \
    definition.h \
    application.h
