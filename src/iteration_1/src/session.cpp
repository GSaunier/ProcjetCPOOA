#include "session.hpp"

// constructeur qui ajoute 3 utilisateurs
Session::Session()
{
    Session::ajouterUtilisateur(new Etudiant("elyautt","coucou"));
    Session::ajouterUtilisateur(new Enseignant("guyome","hello"));
    Session::ajouterUtilisateur(new Admin("arno", "salut"));
}

// ajouter un utilisateur dans le tableau
void Session::ajouterUtilisateur(Utilisateur *user)
{
    Session::tabUser.push_back(*user);
}

// recuperer un utilisateur par son index
Utilisateur Session::getUtilisateur(int index)
{
    return Session::tabUser[index];
}

bool Session::verifMdp(std::string nom, std::string mdp)
{
    bool trouver = false;
    // iteration sur les utilisateurs de tabUser
    for(int unsigned i = 0; i<Session::tabUser.size() ; i++) {
        Utilisateur user = Session::getUtilisateur(i);
        // si on trouve le bon utilisateur
        if(user.getNom() == nom){
            if(user.getMdp() == mdp) {
                trouver = true;
            }
        }
    }
    return trouver;
}
// methode pour connecter un utilisateur, retourne l'utilisateur connecté
Utilisateur Session::connexion(std::string nom, std::string mdp)
{
    Utilisateur user;
    for(int unsigned i = 0; i<Session::tabUser.size() ; i++) {
        user = Session::getUtilisateur(i);
        if(user.getNom() == nom){
            if(user.getMdp() == mdp){
                i = Session::tabUser.size();
            }
        }
    }
    return user;
}
// methode pour inscrire un utilisateur
void Session::inscription(std::string nom, std::string mdp, int type)
{
    switch(type){
        case ETUDIANT :
            Session::ajouterUtilisateur(new Etudiant(nom,mdp));
            break;
        case ENSEIGNANT :
            Session::ajouterUtilisateur(new Enseignant(nom,mdp));
            break;
        case ADMIN :
            Session::ajouterUtilisateur(new Admin(nom,mdp));
            break;
        default :
            break;
    }
}
