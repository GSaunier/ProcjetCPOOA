#ifndef APPLICATION_H
#define APPLICATION_H

#include <QCoreApplication>
#include <iostream>
#include "utilisateur.hpp"
#include "etudiant.hpp"
#include "enseignant.hpp"
#include "admin.hpp"
#include "session.hpp"
#include "definition.h"

/// @brief   Cette méthode déconnecte l'utilisateur.
///
/// @return  Un utilisateur par défaut nommé Aucun utilisateur .
Utilisateur deconnexion(){return Utilisateur();}

#endif // APPLICATION_H
