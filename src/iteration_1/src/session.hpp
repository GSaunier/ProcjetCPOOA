#ifndef SESSION_H
#define SESSION_H
#include <vector>
#include "definition.h"
#include "utilisateur.hpp"
#include "admin.hpp"
#include "enseignant.hpp"
#include "etudiant.hpp"

/** @brief La classe Session est la classe qui va regrouper les
     **        donnees de l'application.
     **
     ** Elle contient un vecteur des utilisateurs de l'application
     ** et une instance de Session.
     **
     ** @version iteration une
     **
     **/
class Session
{

public:
    /// @brief Methode pour recuperer l'instance de Session
    ///
    /// @return instance de la Session
    ///
    /// @see instance.
    static Session& getInstance(){
        static Session instance;
        return instance;
    }
    /// @brief permet de connecter un utilisateur à l'application
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @return l'utilisateur connecté
    /// @see     nom, mdp.
    Utilisateur connexion(std::string nom, std::string mdp);

    /// @brief permet de verifier si une combinaison de nom mot de passe existe
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @return un booleen
    /// @see     nom, mdp.
    bool verifMdp(std::string nom, std::string mdp);

    /// @brief permet d'inscrire un utilisateur à l'application
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    /// @param type le type de l'utilisateur
    ///
    /// @see     nom, mdp.
    void inscription(std::string nom, std::string mdp, int type);

    /// @brief permet d'ajouter un utilisateur à la session
    ///
    /// @param  user l'utilisateur.
    void ajouterUtilisateur(Utilisateur *user);

    /// @brief permet de recuperer un utilisateur à partir de son  index
    ///
    /// @param  index  l'index de l'utilisateur,
    ///
    /// @return l'utilisateur connecté
    Utilisateur getUtilisateur(int index);

private:
    /// @brief Le constructeur est prive car c'est un singleton
    Session();
    /// @brief Le tableau des utilisateurs utilises dans l'application.
    std::vector<Utilisateur> tabUser;
};

#endif // SESSION_H
