#include "application.h"

using namespace std;
/** @example console.cpp
 **
 ** Ce programme teste les classes héritant de @ref utilisateur.
 **
 ** @version @version iteration une
 **
 **/

///
/// @param  argc  nombre de paramètres du programme (non utilisé),
/// @param  argv  paramètres du programme (non utilisé).
///
/// @return  0 (OK).
///
/// @version iteration une
int main()
{
    Session& sess = Session::getInstance();

    cout << "Test instanciation des Utilisateurs\n" << endl;

    Etudiant elyautt("elyautt","coucou");
    cout << elyautt.toString() << endl;

    Admin arno("arno","salut");
    cout << arno.toString() << endl;

    Enseignant guyome("guyome","hello");
    cout << guyome.toString() << endl;

    cout << "\nTest du de l'instanciation du singleton et du remplissage de tabUser\n" << endl;
    cout << sess.getUtilisateur(0).toString() << endl;
    cout << sess.getUtilisateur(1).toString() << endl;
    cout << sess.getUtilisateur(2).toString() << endl;

    if(!sess.verifMdp("efs","fsd")){
        cout << "L'utilisateur n'existe pas" << endl;
    } else {
        cout << "Erreur de verifMdp" << endl;
    }

    cout << "\n Test de connexion et deconnexion : \n" << endl;

    if(sess.verifMdp("elyautt","coucou")){
        cout << "L'utilisateur existe" << endl;
        Utilisateur connect = sess.connexion("elyautt", "coucou");
        cout << "Nom utilisateur : " + connect.getNom() + " Mdp : " + connect.getMdp() << endl;
        connect = deconnexion();
        cout << "Nom utilisateur : " + connect.getNom() + " Mdp : " + connect.getMdp() << endl;
    } else {
        cout << "Erreur de verifMdp" << endl;
    }

    cout << "\nTest de l'inscription :\n" << endl;
    sess.inscription("Test", "world", ADMIN);
    cout << sess.getUtilisateur(3).toString() << endl;

    cout << "\n Affichage des destructeurs : \n" << endl;

    return 0;
}


