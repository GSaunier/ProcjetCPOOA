#include "donnees.hpp"

// constructeur qui ajoute 3 utilisateurs
Donnees::Donnees()
{
    Donnees::ajouterUtilisateur(new Etudiant("elyautt","coucou"));
    Donnees::ajouterUtilisateur(new Enseignant("guyome","hello"));
    Donnees::ajouterUtilisateur(new Admin("arno", "salut"));
}

// ajouter un utilisateur dans le tableau
void Donnees::ajouterUtilisateur(Utilisateur *user)
{
    Donnees::tabUser.push_back(user);
}

bool Donnees::verifMdp(std::string nom, std::string mdp)
{
    bool trouver = false;
    // iteration sur les utilisateurs de tabUser
    for(int unsigned i = 0; i<Donnees::tabUser.size() ; i++) {
        Utilisateur * user = Donnees::getUtilisateur(i);
        // si on trouve le bon utilisateur
        if(user->getNom() == nom){
            if(user->getMdp() == mdp) {
                trouver = true;
            }
        }
    }
    return trouver;
}
// methode pour connecter un utilisateur, retourne l'utilisateur connecté
Utilisateur * Donnees::connexion(std::string nom, std::string mdp)
{
    Utilisateur *user;
    for(int unsigned i = 0; i<Donnees::tabUser.size() ; i++) {
        user = Donnees::getUtilisateur(i);
        if(user->getNom() == nom){
            if(user->getMdp() == mdp){
                i = Donnees::tabUser.size();
            }
        }
    }
    return user;
}
// methode pour inscrire un utilisateur
void Donnees::inscription(std::string nom, std::string mdp, int type)
{
    Utilisateur * user;
    switch(type){
        case ETUDIANT :
            user = new Etudiant(nom,mdp);
            Donnees::ajouterUtilisateur(user);
            break;
        case ENSEIGNANT :
            user = new Enseignant(nom,mdp);
            Donnees::ajouterUtilisateur(user);
            break;
        case ADMIN :
            user = new Admin(nom,mdp);
            Donnees::ajouterUtilisateur(user);
            break;
        default :
            break;
    }
    Donnees::ecrireUser(user);
}

std::vector<Cours*> Donnees::getListeCoursValide(){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(tabCours[i]->isValide()){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}

std::vector<Cours *> Donnees::getListeCoursAttente(){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(!tabCours[i]->isValide()){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}

std::vector<Cours*> Donnees::getCoursEnseignant(std::string nom){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(tabCours[i]->getNomEnseignant() == nom){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}

void Donnees::ecrireUser(Utilisateur *u){
    std::ofstream fichierUser("users.txt",std::ios::app);
    fichierUser << u->getNom() << std::endl;
    fichierUser << u->getMdp() << std::endl;
    fichierUser << u->getType() << std::endl;
}

// fonction qui permet de lire le fichier users.txt pour implementer le tableau users
void Donnees::lireUser()
{

    std::ifstream fichier;
    fichier.open ("users.txt", std::ifstream::in);
            if(fichier)  // si l'ouverture a réussi
            {
                int type;
                std::string nom, mdp;
                std::string ligne;
                while(std::getline(fichier, ligne))  // tant que l'on peut mettre la ligne dans "contenu"
                {
                    nom = ligne;
                    std::getline(fichier, ligne);
                    mdp = ligne;
                    std::getline(fichier, ligne);
                    type = atoi(ligne.c_str());
                    std::cout << nom << mdp << std::endl;
                    if (type==ENSEIGNANT){
                        Enseignant * ens = new Enseignant(nom,mdp) ;
                        ajouterUtilisateur(ens);
                    }
                    if (type == ETUDIANT){
                        Etudiant* etu = new Etudiant(nom,mdp);
                         ajouterUtilisateur(etu);
                    }
                    if (type == ADMIN){
                        Admin* admin = new Admin(nom,mdp);
                        ajouterUtilisateur(admin);
                    }
                }
                fichier.close();  // on ferme le fichier
            }
            else  // sinon
                    std::cerr << "Impossible d'ouvrir le fichier !" << std::endl;
}

