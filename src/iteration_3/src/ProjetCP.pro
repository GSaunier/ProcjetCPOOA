QT += core
QT -= gui

CONFIG += c++11

TARGET = ProjetCP
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    application.cpp \
    main.cpp \
    donnees.cpp \
    cours.cpp \
    enseignant.cpp

HEADERS += \
    admin.hpp \
    enseignant.hpp \
    etudiant.hpp \
    utilisateur.hpp \
    definition.h \
    application.h \
    donnees.hpp \
    cours.h
