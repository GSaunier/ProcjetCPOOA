#include <QCoreApplication>
#include <iostream>
#include "utilisateur.hpp"
#include "etudiant.hpp"
#include "enseignant.hpp"
#include "admin.hpp"
#include "donnees.hpp"
#include "definition.h"
#include "application.h"

using namespace std;
/** @example console.cpp
 **
 ** Ce programme teste les classes héritant de @ref utilisateur.
 **
 ** @version @version iteration une
 **
 **/

///
/// @param  argc  nombre de paramètres du programme (non utilisé),
/// @param  argv  paramètres du programme (non utilisé).
///
/// @return  0 (OK).
///
/// @version iteration 2 a

int main(){
    Donnees& donnees = Donnees::getInstance();

    cout << "Test instanciation des Utilisateurs\n" << endl;

    Etudiant elyautt("elyautt","coucou");
    cout << elyautt.toString() << endl;

    Admin arno("arno","salut");
    cout << arno.toString() << endl;

    Enseignant guyome("guyome","hello");
    cout << guyome.toString() << endl;

    cout << "\nTest du de l'instanciation du singleton et du remplissage de tabUser\n" << endl;
    cout << donnees.getUtilisateur(0)->toString() << endl;
    cout << donnees.getUtilisateur(1)->toString() << endl;
    cout << donnees.getUtilisateur(2)->toString() << endl;

    if(!donnees.verifMdp("efs","fsd")){
        cout << "L'utilisateur n'existe pas" << endl;
    } else {
        cout << "Erreur de verifMdp" << endl;
    }

    cout << "\n Test de connexion et deconnexion : \n" << endl;
    Application app = Application();
    if(donnees.verifMdp("elyautt","coucou")){
        cout << "L'utilisateur existe" << endl;
        app.connexion("elyautt", "coucou");
        cout << "Nom utilisateur : " + app.getUser()->getNom() + " Mdp : " + app.getUser()->getMdp() + " Connexion : " + std::to_string(app.isConnect()) << endl;
        app.deconnexion();
        cout << "Connexion : " + std::to_string(app.isConnect()) << endl;
    } else {
        cout << "Erreur de verifMdp" << endl;
    }

    cout << "\nTest de l'inscription :\n" << endl;
    donnees.inscription("Test", "world", ADMIN);
    cout << donnees.getUtilisateur(3)->toString() << endl;

    cout << "\nTest ajout de cours :\n" << endl;

    donnees.ajouterCours(new Cours("moi","12/08/2016","13/12/2018",10));

    cout << donnees.getListeCoursAttente()[0]->toString() << endl;

    app.connexion("guyome","hello");

    Utilisateur *user = app.getUser();

    Enseignant * ens = (Enseignant *)user;

    ens->ajouterCours("14/03/2017","23/12/2019",11);

    cout << donnees.getListeCoursAttente()[1]->toString() << endl;

    app.connexion("arno", "salut");

    user = app.getUser();

    Admin * adm = (Admin *)user;

    adm->validerCours(donnees.getListeCoursAttente()[1]);

    cout << donnees.getListeCoursValide()[0]->toString() << endl;

    app.connexion("guyome","hello");

    user = app.getUser();

    ens = (Enseignant *)user;

    ens->modifierCours(donnees.getCoursEnseignant(ens->getNom())[0], "Salut je suis le cours\n");

    cout << donnees.getCoursEnseignant(ens->getNom())[0]->getContenu() << endl;

    app.connexion("elyautt","coucou");
    user = app.getUser();
    Etudiant * etu = (Etudiant *)user;

    etu->inscriptionCours(donnees.getListeCoursValide()[0]);

    cout << donnees.getListeCoursValide()[0]->getTabEtudiant()[0] << "\n" << endl;

    cout << "Test ecrireUser()\n" << endl;
    donnees.ecrireUser(new Admin("guiguite","estla"));
    donnees.lireUser();
    cout << donnees.getUtilisateur(5)->toString() << endl;

    //Supprime le fichier créé a la fin du test
    std::remove("users.txt");

    return 0;
}
