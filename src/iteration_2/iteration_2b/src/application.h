#ifndef APPLICATION_H
#define APPLICATION_H

#include "utilisateur.hpp"
#include "etudiant.hpp"
#include "enseignant.hpp"
#include "admin.hpp"
#include "donnees.hpp"
#include "definition.h"

class Application{
    private:
        /// @brief l'utilisateur instancité a l'application.
        Utilisateur *user;

        /// @brief boolen qui dit si l'utilisateur est connecté a l'application.
        bool connecter;

    public:
        Application():connecter(false){}

        /// @brief   Cette méthode déconnecte l'utilisateur.
        void deconnexion(){connecter = false;}

        /// @brief   Cette méthode connecte l'utilisateur.
        void connexion(std::string nom, std::string mdp);

        /// @brief   Cette méthode retourne l'utilisateur de l'application.
        Utilisateur *getUser(){return user;}

        /// @brief   getter pour savoir si l'utilisateur est connecté.
        bool isConnect(){return connecter;}
};



#endif // APPLICATION_H
