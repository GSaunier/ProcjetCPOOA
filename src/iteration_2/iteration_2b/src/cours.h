#ifndef COURS_H
#define COURS_H

#include <iostream>
#include <ctime>
#include <vector>

/** @brief La classe Cours représente tout les cours.
 **
 **
 ** @version iteration deux
 **
 **/
class Cours
{
    private:
        /// @brief tableau des étudiants inscris au cours.
        std::vector<std::string> tabEtudiant;
        /// @brief nom de l'enseignant du cours.
        std::string nom;
        /// @brief date de debut du cours.
        std::string dateDeb;
        /// @brief date de fin.
        std::string dateFin;
        /// @brief contenu du cours.
        std::string contenu;
        /// @brief nombre de place du cours.
        int nbPlace;
        /// @brief boleen qui dit s'il à été validé ou non.
        bool valide;
    public:
        Cours(std::string n,std::string deb,std::string fin, int nb):nom(n),dateDeb(deb),dateFin(fin),nbPlace(nb),valide(false){}

        /// @brief   Getter pour la validite du cours.
        ///
        /// @return  booleen valide.
        ///
        /// @see     valide.
        bool isValide(){return valide;}

        /// @brief   Getter pour le nom de l'enseignant propriétaire.
        ///
        /// @return  booleen valide.
        ///
        /// @see     valide.
        std::string getNomEnseignant(){return nom;}

        /// @brief   Getter pour la date de debut du cours.
        ///
        /// @return  date de debut du cours.
        ///
        /// @see     dateDeb.
        std::string getDateDeb(){return dateDeb;}

        /// @brief   Getter pour la date de fin du cours.
        ///
        /// @return  date de fin du cours.
        ///
        /// @see     dateFin.
        std::string getDateFin(){return dateFin;}

        /// @brief   methode pour valider le cours.
        ///
        /// @see     valide.
        void valider(){valide = true;}

        /// @brief   Getter pour le contenu du cours.
        ///
        /// @return  contenu du cours.
        ///
        /// @see     contenu.
        std::string getContenu(){return contenu;}

        /// @brief   Cette méthode ajoute un étudiant à la liste des étudiants du cours.
        ///
        /// @see     tabEtudiant.
        void ajouterEtudiant(std::string nom){tabEtudiant.push_back(nom);}

        /// @brief   Cette méthode donne le tableau d'étudiants.
        ///
        /// @return  le tableau d'étudiants.
        ///
        /// @see     tabEtudiant.
        std::vector<std::string> getTabEtudiant(){return tabEtudiant;}

        /// @brief   Cette méthode donne le nombre de place libres.
        ///
        /// @return  le nombre de place libre.
        ///
        /// @see     nbPlace.
        int getPlaceLibre(){return nbPlace - tabEtudiant.size();}

        /// @brief   Setter pour le contenu.
        ///
        /// @see     contenu.
        void setContenu(std::string src){contenu = src;}

        /// @brief   Cette méthode donne les attributs du cours.
        ///
        /// @return  les attributs du cours (dans une chaîne de caractères C++).
        ///
        /// @see     nom,
        /// @see     dateDeb,
        /// @see     dateFin.
        /// @see     valide.
        std::string toString(){return "Cours de " + nom + " Debut: " + dateDeb + " Fin: " + dateFin + " Valide: "+std::to_string(valide)+".\n";}
};

#endif // COURS_H
