#ifndef DONNEES_H
#define DONNEES_H
#include <vector>
#include "definition.h"
#include "utilisateur.hpp"
#include "admin.hpp"
#include "enseignant.hpp"
#include "etudiant.hpp"
#include "cours.h"
#include <map>
#include <algorithm>

/** @brief La classe Donnees est la classe qui va regrouper les
     **        donnees de l'application.
     **
     ** Elle contient un vecteur des utilisateurs de l'application
     ** et une instance de Donnees.
     **
     ** @version iteration deux
     **
     **/
class Donnees
{

public:
    /// @brief Methode pour recuperer l'instance de Donnees
    ///
    /// @return instance de la Donnees
    ///
    /// @see instance.
    static Donnees& getInstance(){
        static Donnees instance;
        return instance;
    }
    /// @brief permet de connecter un utilisateur à l'application
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @return l'utilisateur connecté
    /// @see     nom, mdp.
    Utilisateur * connexion(std::string nom, std::string mdp);

    /// @brief permet de verifier si une combinaison de nom mot de passe existe
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @return un booleen
    /// @see     nom, mdp.
    bool verifMdp(std::string nom, std::string mdp);

    /// @brief permet d'inscrire un utilisateur à l'application
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    /// @param type le type de l'utilisateur
    ///
    /// @see     nom, mdp.
    void inscription(std::string nom, std::string mdp, int type);

    /// @brief permet d'ajouter un utilisateur aux Donnees
    ///
    /// @param  user l'utilisateur.
    void ajouterUtilisateur(Utilisateur *user);

    /// @brief permet de recuperer un utilisateur à partir de son  index
    ///
    /// @param  index  l'index de l'utilisateur,
    ///
    /// @return l'utilisateur connecté
    Utilisateur * getUtilisateur(int index){return &tabUser[index];}

    /// @brief   Cette méthode donne les attributs du cours.
    ///
    /// @return  les attributs du cours (dans une chaîne de caractères C++).
    ///
    /// @see     nom,
    /// @see     dateDeb,
    /// @see     dateFin.
    /// @see     valide.
    void ajouterCours(Cours * c){tabCours.push_back(c);}

    /// @brief   Cette méthode donne la liste des cours valide.
    ///
    /// @return  les cours valides.
    std::vector<Cours*> getListeCoursValide();

    /// @brief   Cette méthode donne la liste des cours en attente de validation.
    ///
    /// @return  les cours en attente.
    std::vector<Cours*> getListeCoursAttente();

    /// @brief   Cette méthode donne la liste des cours d'un enseignant.
    ///
    /// @param   nom de l'enseignant.
    ///
    /// @return  les cours de l'enseignant.
    std::vector<Cours*> getCoursEnseignant(std::string nom);

private:
    /// @brief Le constructeur est prive car c'est un singleton
    Donnees();
    /// @brief Le tableau des enseignants utilises dans l'application.
    std::vector<Utilisateur> tabUser;
    /// @brief Le tableau des cours utilises dans l'application.
    std::vector<Cours*> tabCours;
};

#endif // Donnees_H
