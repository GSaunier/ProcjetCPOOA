#ifndef UTILISATEUR_H
#define UTILISATEUR_H
#include <iostream>

/** @brief La classe utilisateur est la classe abstraite de tous
 **        les utilisateurs de l'application.
 **
 ** Elle contient deux chaines de caracteres pour le nom et le type
 **
 ** @version iteration deux
 **
 **/
class Utilisateur
{
protected:

    /// @brief Le nom de l'utilisateur est une chaine de caractere.
    std::string nom;
    /// @brief le mot de passe de l'utilisateur est une chaine de caractere.
    std::string mdp;
    /// @brief Le type de l'utilisateur est une chaine de caractere, ce sera soit etudiant - enseignant - admin.
    int type;



public:

    /// @brief   Cette méthode donne les attributs de l'utilisateur.
    ///
    /// @return  les attributs l'utilisateur (dans une chaîne de caractères C++).
    ///
    /// @see     nom,
    /// @see     mdp,
    /// @see     type.
    std::string toString(){
        return "nom : " +nom+", type : " + std::to_string(type) +  ", mdp : "+mdp ;
    }

    /// @brief Le constructeur permet de remplir les champs
    ///        de la classe en fonction des paramètres passés.
    ///
    /// Ce constructeur est protégé pour que la classe utilisateur
    /// reste abstraite :  seules ses classes filles peuvent être
    /// instanciées (cela restreint les utilisateurs qu'on peut créer).
    ///
    /// @param  n  le nom de l'utilisateur,
    /// @param  p  le type de l'utilisateur cree.
    /// @param  m  le mot de passe de l'utilisateur
    ///
    /// @see     Nom, Prix.
    Utilisateur(std::string n,std::string m, int p):nom(n),mdp(m),type(p){}

    /// @brief Le constructeur vide
    ///     créer un utilisateur par défaut avec un mot de passe par defaut
    /// @see nom,mdp.
    Utilisateur():nom("Aucun Utilisateur"),mdp("Aucun Utilisateur"),type(-1){}

    /// @brief   Cette méthode donne le nom de l'utilisateur.
    ///
    /// @return  le nom de l'utilisateur (dans une chaîne de caractères C++).
    ///
    /// @see     nom.
    std::string getNom(){return nom;}

    /// @brief   Cette méthode donne le mot de passe de l'utilisateur.
    ///
    /// @return  le mot de passe de l'utilisateur (dans une chaîne de caractères C++).
    ///
    /// @see     mdp.
    std::string getMdp(){return mdp;}

    /// @brief   Cette méthode donne le type de l'utilisateur.
    ///
    /// @return  le type de l'utilisateur (dans une chaîne de caractères C++).
    ///
    /// @see     type.
    int getType(){return type;}

    virtual ~Utilisateur(){}

private:


};

#endif // UTILISATEUR_H
