#ifndef Enseignant_H
#define Enseignant_H
#include "definition.h"
#include "utilisateur.hpp"
#include "donnees.hpp"
#include <iostream>

/** @brief La classe enseignant est un utilisateur de l'application.
 **
 ** Elle utilise le constructeur d'utilisateur avec le bon parametre
 ** et signale dans std::cout l'appel
 ** à son destructeur
 **
 ** @version iteration deux
 **
 **/
class Enseignant : public Utilisateur
{
public:
    /// @brief Le constructeur appelle celui d'utilisateur
    ///        avec les bons paramètres.
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @see     nom, mdp.
    Enseignant(std::string n, std::string m) : Utilisateur(n,m,ENSEIGNANT){}

    /// @brief   Cette méthode ajoute un cours.
    ///
    /// @param deb date de debut du cours
    /// @param fin date de fin du cours
    /// @param nb nombre de place du cours
    void ajouterCours(std::string deb, std::string fin, int nb);

    /// @brief   Cette méthode modifie un cours.
    ///
    /// @param c cours a modifier,
    /// @param src nouveau contenu du cours
    void modifierCours(Cours * c, std::string src){c->setContenu(src);}


    /// @brief Le destructeur signale dans std::cout
    ~Enseignant(){
        std::cout << "~enseignant()" << std::endl;
    }
};

#endif // Enseignant_H
