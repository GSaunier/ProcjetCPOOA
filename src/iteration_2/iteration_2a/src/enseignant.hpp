#ifndef Enseignant_H
#define Enseignant_H
#include "definition.h"
#include "utilisateur.hpp"
#include "donnees.hpp"
#include <iostream>

/** @brief La classe enseignant est un utilisateur de l'application.
 **
 ** Elle utilise le constructeur d'utilisateur avec le bon parametre
 ** et signale dans std::cout l'appel
 ** à son destructeur
 **
 ** @version iteration une
 **
 **/
class Enseignant : public Utilisateur
{
public:
    /// @brief Le constructeur appelle celui d'utilisateur
    ///        avec les bons paramètres.
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @see     nom, mdp.
    Enseignant(std::string n, std::string m) : Utilisateur(n,m,ENSEIGNANT){}

    void ajouterCours(std::string deb, std::string fin, int nb);

    std::string getNom(){return Utilisateur::getNom();}

    /// @brief Le destructeur signale dans std::cout
    ~Enseignant(){
        std::cout << "~enseignant()" << std::endl;
    }
};

#endif // Enseignant_H
