#ifndef COURS_H
#define COURS_H

#include <iostream>
#include <ctime>

class Cours
{
    private:
        std::string nom;
        std::string dateDeb;
        std::string dateFin;
        int nbPlace;
        bool valide;
    public:
        Cours(std::string n,std::string deb,std::string fin, int nb):nom(n),dateDeb(deb),dateFin(fin),nbPlace(nb),valide(false){}
        bool isValide(){return valide;}
        std::string getNomEnseignant(){return nom;}
        std::string getDateDeb(){return dateDeb;}
        std::string getDateFin(){return dateFin;}
        void valider(){valide = true;}
        std::string toString(){return "Cours de " + nom + " Debut: " + dateDeb + " Fin: " + dateFin + " Valide: "+std::to_string(valide)+".\n";}
};

#endif // COURS_H
