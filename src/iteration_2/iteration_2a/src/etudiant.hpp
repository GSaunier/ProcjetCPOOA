#ifndef ETUDIANT_H
#define ETUDIANT_H
#include "definition.h"

/** @brief La classe etudiant est un utilisateur de l'application.
 **
 ** Elle utilise le constructeur d'utilisateur avec le bon parametre
 ** et signale dans std::cout l'appel
 ** à son destructeur
 **
 ** @version iteration une
 **
 **/
class Etudiant : public Utilisateur
{
public:
    /// @brief Le constructeur appelle celui d'utilisateur
    ///        avec les bons paramètres.
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @see     nom, mdp.
    Etudiant(std::string n, std::string m) : Utilisateur(n,m,ETUDIANT){}

    /// @brief Le destructeur signale dans std::cout
    ~Etudiant(){
        std::cout << "~etudiant()" << std::endl;
    }
};

#endif // ETUDIANT_H
