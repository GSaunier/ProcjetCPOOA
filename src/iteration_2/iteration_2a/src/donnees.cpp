#include "donnees.hpp"

// constructeur qui ajoute 3 utilisateurs
Donnees::Donnees()
{
    Donnees::ajouterUtilisateur(new Etudiant("elyautt","coucou"));
    Donnees::ajouterUtilisateur(new Enseignant("guyome","hello"));
    Donnees::ajouterUtilisateur(new Admin("arno", "salut"));
}

// ajouter un utilisateur dans le tableau
void Donnees::ajouterUtilisateur(Utilisateur *user)
{
    Donnees::tabUser.push_back(*user);
}

bool Donnees::verifMdp(std::string nom, std::string mdp)
{
    bool trouver = false;
    // iteration sur les utilisateurs de tabUser
    for(int unsigned i = 0; i<Donnees::tabUser.size() ; i++) {
        Utilisateur * user = Donnees::getUtilisateur(i);
        // si on trouve le bon utilisateur
        if(user->getNom() == nom){
            if(user->getMdp() == mdp) {
                trouver = true;
            }
        }
    }
    return trouver;
}
// methode pour connecter un utilisateur, retourne l'utilisateur connecté
Utilisateur * Donnees::connexion(std::string nom, std::string mdp)
{
    Utilisateur *user;
    for(int unsigned i = 0; i<Donnees::tabUser.size() ; i++) {
        user = Donnees::getUtilisateur(i);
        if(user->getNom() == nom){
            if(user->getMdp() == mdp){
                i = Donnees::tabUser.size();
            }
        }
    }
    return user;
}
// methode pour inscrire un utilisateur
void Donnees::inscription(std::string nom, std::string mdp, int type)
{
    switch(type){
        case ETUDIANT :
            Donnees::ajouterUtilisateur(new Etudiant(nom,mdp));
            break;
        case ENSEIGNANT :
            Donnees::ajouterUtilisateur(new Enseignant(nom,mdp));
            break;
        case ADMIN :
            Donnees::ajouterUtilisateur(new Admin(nom,mdp));
            break;
        default :
            break;
    }
}

std::vector<Cours*> Donnees::getListeCoursValide(){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(tabCours[i]->isValide()){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}

std::vector<Cours *> Donnees::getListeCoursAttente(){
    std::vector<Cours*> tab;
    for(int unsigned i = 0; i < tabCours.size(); i++){
        if(!tabCours[i]->isValide()){
            tab.push_back(tabCours[i]);
        }
    }
    return tab;
}
