@startuml
title Diagramme de classe

hide empty members

package "user" #DDDDDD{
    abstract class Utilisateur {
        - String mdp
        - String nom
        - int type
        --
        .. Constructeur ..
        + Utilisateur()
        + Utilisateur(String nom, String mdp, int type)
        .. Methode ..
        + void deconnexion()
        + int getType()
        + String toString()
    }
    class Etudiant{
    }
    class Enseignant{
        .. Methode ..
        + void ajouterCours(string deb, string fin, int places)
    }
    class Admin{
        .. Methode ..
        + void validerCours(Cours * cours)
    }
    
    
    Utilisateur <|-- Enseignant
    Utilisateur <|-- Etudiant
    Utilisateur <|-- Admin

}
class Cours{
-string nomEnseignant
-string dateDeb
-string dateFin
-int nbPlace
-bool valide

.. Constructeur ..
+ Cours(string nom, string deb, string fin, int place)

        .. Methode ..
        + bool isCourValide()
        + void valider()

}
Enseignant -- "*" Cours
Cours -- "*" Etudiant
class Donnees{
        - static Donnees instance
        - vector<Utilisateur> tabUser
        - vector<Cours*> tabCours
        --
        .. Constructeur ..
        - Donnees()
        .. Methode pour Singleton ..
        + Donnee getInstance()
        .. Methode ..
        + Utilisateur * connexion(String nom, String mdp)
        + Utilisateur * getUtilisateur(int index)
        + bool verifMdp(String nom, String mdp)
        + void inscription(String nom, String mdp)
        + void ajouterCour (Cours * cours)
        + vector<Cours*> getListeCoursValide ()
        + vector<Cours*> getListeCoursAttente ()
}

note bottom of Donnees #FFAAAA
    Design Pattern :
    <b>Singleton</b>
    end note

class Application{
    - Utilisateur * user
    - bool
    .. Constructeur ..
    - Application()
    .. Méthodes ..
    + void deconnexion()
    + void connexion(string nom, string mdp)
    + Utilisateur * getUser()
    + bool isConnect()
}

Application - "1" Utilisateur
@enduml
