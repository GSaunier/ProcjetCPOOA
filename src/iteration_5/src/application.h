#ifndef APPLICATION_H
#define APPLICATION_H

#include "utilisateur.hpp"
#include "etudiant.hpp"
#include "enseignant.hpp"
#include "admin.hpp"
#include "donnees.hpp"
#include "definition.h"

class Application{
    private:
        Utilisateur *user;
        bool connecter;

    public:
        Application():connecter(false){}

        /// @brief   Cette méthode déconnecte l'utilisateur.
        void deconnexion(){connecter = false;}

        /// @brief   Cette méthode connecte l'utilisateur.
        void connexion(std::string nom, std::string mdp);

        Utilisateur *getUser(){return user;}

        bool isConnect(){return connecter;}
};



#endif // APPLICATION_H
