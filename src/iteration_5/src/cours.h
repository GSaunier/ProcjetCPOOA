#ifndef COURS_H
#define COURS_H

#include <iostream>
#include <ctime>
#include <vector>

class Cours
{
    private:
        std::vector<std::string> tabEtudiant;
        std::string nom;
        std::string dateDeb;
        std::string dateFin;
        std::string contenu;
        int nbPlace;
        bool valide;
    public:
        Cours(std::string n,std::string deb,std::string fin, int nb):nom(n),dateDeb(deb),dateFin(fin),nbPlace(nb),valide(false){}
        bool isValide(){return valide;}
        std::string getNomEnseignant(){return nom;}
        std::string getDateDeb(){return dateDeb;}
        std::string getDateFin(){return dateFin;}
        void valider(){valide = true;}
        std::string getContenu(){return contenu;}
        void ajouterEtudiant(std::string nom){tabEtudiant.push_back(nom);}
        std::vector<std::string> getTabEtudiant(){return tabEtudiant;}
        int getPlaceLibre(){return nbPlace - tabEtudiant.size();}
        void setContenu(std::string src){contenu = src;}
        std::string toString(){return "Cours de " + nom + " Debut: " + dateDeb + " Fin: " + dateFin + " Valide: "+std::to_string(valide)+".\n";}
        void supprEtudiant(std::string nom);
};

#endif // COURS_H
