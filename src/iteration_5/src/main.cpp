#include <QCoreApplication>
#include <iostream>
#include "utilisateur.hpp"
#include "etudiant.hpp"
#include "enseignant.hpp"
#include "admin.hpp"
#include "donnees.hpp"
#include "definition.h"
#include "application.h"
#include <QApplication>

using namespace std;
/** @example console.cpp
 **
 ** Ce programme teste les classes héritant de @ref utilisateur.
 **
 ** @version @version iteration une
 **
 **/

///
/// @param  argc  nombre de paramètres du programme (non utilisé),
/// @param  argv  paramètres du programme (non utilisé).
///
/// @return  0 (OK).
///
/// @version iteration 2 a

int main(){

    QApplication app(argc, argv);



    return app.exec();
}
