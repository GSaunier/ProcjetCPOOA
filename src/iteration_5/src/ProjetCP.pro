QT += core
QT += widgets

CONFIG += c++11

TARGET = ProjetCP
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    application.cpp \
    main.cpp \
    donnees.cpp \
    cours.cpp \
    enseignant.cpp \
    admin.cpp \
    fenetre.cpp

HEADERS += \
    admin.hpp \
    enseignant.hpp \
    etudiant.hpp \
    utilisateur.hpp \
    definition.h \
    application.h \
    donnees.hpp \
    cours.h \
    ressource.hpp \
    devoir.hpp \
    controle.hpp \
    fichier.hpp \
    texte.hpp \
    fenetre.h
