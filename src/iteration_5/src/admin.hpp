#ifndef ADMIN_H
#define ADMIN_H

#include "definition.h"
#include "cours.h"
#include "donnees.hpp"

/** @brief La classe admin est un utilisateur de l'application.
 **
 ** Elle utilise le constructeur d'utilisateur avec le bon parametre
 ** et signale dans std::cout l'appel
 ** à son destructeur
 **
 ** @version iteration une
 **
 **/

class Admin : public Utilisateur
{
public:
    /// @brief Le constructeur appelle celui d'utilisateur
    ///        avec les bons paramètres.
    /// @param  n  le nom de l'utilisateur,
    /// @param  m  le mot de passe l'utilisateur cree.
    ///
    /// @see     nom, mdp.
    Admin(std::string n, std::string m) : Utilisateur(n,m,ADMIN){}

    void validerCours(Cours *c){c->valider();}

    void supprimerUser(Utilisateur * user);

    /// @brief Le destructeur signale dans std::cout
    ~Admin(){
        std::cout << "~admin()" << std::endl;
    }
};

#endif // ADMIN_H
