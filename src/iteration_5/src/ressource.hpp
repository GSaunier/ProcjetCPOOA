#ifndef RESSOURCE_H
#define RESSOURCE_H
#include <iostream>


class Ressource
{

protected:
     std::string nom;
     std::string date;
     int type;

public:

     Ressource(std::string n,std::string d, int t):nom(n),date(d),type(t){}

private:

}

#endif // RESSOURCE_H
